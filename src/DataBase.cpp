//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include "../include/DataBase.h"
#include <iostream>

DataBase::DataBase() {

    /* Open database */
    sqlite3_open("../DataBase.db", &this->db);
}

DataBase::~DataBase() {

    sqlite3_close(this->db);
}

std::vector<Item> DataBase::GetItems() {

    int rc;

    std::vector<Item> items;
    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(this->db, "SELECT * FROM main.items", -1, &stmt, nullptr);

    if (rc != SQLITE_OK) {
        std::cout << "SELECT failed: " << sqlite3_errmsg(this->db) << std::endl;

        //return ...; // or throw
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        int id = sqlite3_column_int(stmt, 0);
        int attack = sqlite3_column_int(stmt, 2);
        int health = sqlite3_column_int(stmt, 3);
        int armor = sqlite3_column_int(stmt, 4);
        bool key = sqlite3_column_int(stmt, 6);
        int idUnit = sqlite3_column_int(stmt, 7);


        const char *name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
        const char *description = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 5));

        Item item(id, health, attack, armor, std::string(name), std::string(description), key, idUnit);
        items.push_back(item);
    }

    if (rc != SQLITE_DONE)
        std::cout << "SELECT failed: " << sqlite3_errmsg(this->db) << std::endl;

    sqlite3_finalize(stmt);

    return items;
}

std::vector<Unit> DataBase::GetUnits() {

    int rc;

    std::vector<Unit> units;
    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(this->db, "SELECT * FROM main.units", -1, &stmt, nullptr);

    if (rc != SQLITE_OK) {
        std::cout << "SELECT failed: " << sqlite3_errmsg(this->db) << std::endl;

        //return ...; // or throw
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        int id = sqlite3_column_int(stmt, 0);
        const char *name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
        int health = sqlite3_column_int(stmt, 2);
        int attack = sqlite3_column_int(stmt, 3);
        int armor = sqlite3_column_int(stmt, 4);
        int role = sqlite3_column_int(stmt, 5);
        int roomId = sqlite3_column_int(stmt, 6);

        Unit unit(id, role, health, attack, armor, roomId, name);
        units.push_back(unit);
    }

    if (rc != SQLITE_DONE)
        std::cout << "SELECT failed: " << sqlite3_errmsg(this->db) << std::endl;

    sqlite3_finalize(stmt);

    return units;
}

std::vector<Room> DataBase::GetRooms() {

    int rc;

    std::vector<Room> rooms;
    sqlite3_stmt *stmt;
    rc = sqlite3_prepare_v2(this->db, "SELECT * FROM main.rooms", -1, &stmt, nullptr);

    if (rc != SQLITE_OK) {
        std::cout << "SELECT failed: " << sqlite3_errmsg(this->db) << std::endl;

        //return ...; // or throw
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        int id = sqlite3_column_int(stmt, 0);
        const char *description = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
        int contMob = sqlite3_column_int(stmt, 2);
        const char *name = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 3));

        Room room(id, contMob, description, name);
        rooms.push_back(room);
    }

    if (rc != SQLITE_DONE)
        std::cout << "SELECT failed: " << sqlite3_errmsg(this->db) << std::endl;

    sqlite3_finalize(stmt);

    return rooms;
}
