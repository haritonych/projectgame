//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include "../include/Item.h"

Item::Item() {
    this->SetId(0);
    this->SetName("not exist");
    this->SetPlusHealth(0);
    this->SetPlusAttack(0);
    this->SetPlusArmor(0);
    this->SetDescription("");
    this->SetKey(false);
    this->SetIdUnit(0);
}

Item::Item(int id, int health, int attack, int armor, std::string name, std::string description, bool key, int idUnit) {
    this->SetId(id);
    this->SetName(name);
    this->SetPlusHealth(health);
    this->SetPlusAttack(attack);
    this->SetPlusArmor(armor);
    this->SetDescription(description);
    this->SetKey(key);
    this->SetIdUnit(idUnit);
}

Item::~Item() {

}

std::string Item::GetShortInfo() {

    std::string shortInfo;

    shortInfo = "";
    if (this->GetPlusHealth() > 0) shortInfo += " +" +  std::to_string(this->GetPlusHealth()) + " HP. ";
    if (this->GetPlusArmor() > 0) shortInfo += " +" + std::to_string(this->GetPlusArmor()) + " Armor. ";
    if (this->GetPlusAttack() > 0) shortInfo += " +" + std::to_string(this->GetPlusAttack()) + " Attack. ";
    if (this->IsKey()) shortInfo += " Open the door to next room";

    if (this->IsEmpty()) {
        return "X_X";
    }
    else {
        if (this->IsKey())
            return "\n\tMagic Key, open the door to next room";
        else
            return "\n\t(" + std::to_string(this->GetId()) + ") Name: " + this->GetName() + "\n\t\tStats: " + shortInfo;
    }
}

bool Item::IsEmpty() {

    return this->GetId() == 0;
}

void Item::SetDescription(std::string description) {

    this->m_description = description;
}

std::string Item::GetDescription() {

    return this->m_description;
}

void Item::SetName(std::string name) {

    this->m_name = name;
}

void Item::SetId(int id) {

    this->m_id = id;
}

void Item::SetPlusHealth(int health) {

    this->m_plusHealth = health;
}

void Item::SetPlusAttack(int attack) {

    this->m_plusAttack = attack;
}

void Item::SetPlusArmor(int armor) {

    this->m_plusArmor = armor;
}

void Item::SetKey(bool key) {

    this->m_key = key;
}

std::string Item::GetName() {

    return this->m_name;
}

int Item::GetId() {

    return this->m_id;
}

int Item::GetPlusHealth() {

    return this->m_plusHealth;
}

int Item::GetPlusArmor() {

    return this->m_plusArmor;
}

int Item::GetPlusAttack() {

    return this->m_plusAttack;
}

bool Item::IsKey() {

    return this->m_key;
}

void Item::SetIdUnit(int idUnit) {

    this->m_id_unit = idUnit;
}

int Item::GetIdUnit() {

    return this->m_id_unit;
}

Item& Item::operator=(Item rightItem) {

    this->SetId(rightItem.GetId());
    this->SetName(rightItem.GetName());
    this->SetDescription(rightItem.GetDescription());
    this->SetPlusAttack(rightItem.GetPlusAttack());
    this->SetPlusArmor(rightItem.GetPlusArmor());
    this->SetPlusHealth(rightItem.GetPlusHealth());
    this->SetKey(rightItem.IsKey());

    return *this;
}