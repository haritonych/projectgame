//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include <iostream>
#include "../include/Unit.h"

Unit::Unit() {

    this->SetName("not exist");
    this->SetId(0);
    this->SetRole(0);
    this->SetHealth(0);
    this->SetAttack(0);
    this->SetArmor(0);
    this->SetCurrentRoom(0);
}

Unit::Unit(int id, int role, int health, int attack, int armor, int roomId, std::string name) {

    this->SetName(name);
    this->SetId(id);
    this->SetRole(role);
    this->SetHealth(health);
    this->SetAttack(attack);
    this->SetArmor(armor);
    this->SetCurrentRoom(roomId);
}

void Unit::CreateHero(std::string heroName) {
    this->SetName(heroName);
    this->SetRole(this->ROLE_HERO);
    this->SetHealth(2000);
    this->SetArmor(5);
    this->SetAttack(150);
    this->SetCurrentRoom(0);
}

void Unit::AddItemToInventory(Item item) {

    this->m_inventory.AddItem(item);
}

void Unit::DropItemFromInventory(Item item) {

    this->m_inventory.DropItem(item);
}

bool Unit::SetItem(Item item) {

    this->m_item = item;

    this->SetAttack(this->GetAttack() + item.GetPlusAttack());
    this->SetArmor(this->GetArmor() + item.GetPlusArmor());
    this->SetHealth(this->GetHealth() + item.GetPlusHealth());

    return true;
}

std::string Unit::RenderInventory() {

    std::string infoInventory;
    int size = int(this->m_inventory.m_items.size());
    int i;

    infoInventory = "";
    for (i = 0; i < size; i++) {
        infoInventory += this->m_inventory.m_items[i].GetShortInfo();
    }

    if (this->m_inventory.IsEmpty())
        return "You dont have items yet ):";
    else
        return infoInventory;
}

void Unit::EquipItemFromInventory(int itemId) {

    this->SetItem(this->GetItemById(itemId));
}

Item Unit::GetItemById(int itemId) {
    int size = int(this->GetInventory().m_items.size());
    int i;

    for (i = 0; i < size; i++) {
        if (this->GetInventory().m_items[i].GetId() == itemId)
            break;
    }

    return this->GetInventory().m_items[i];
}

int Unit::GetKeyFromInventory() {
    int size = int(this->GetInventory().m_items.size());
    int i;

    for (i = 0; i < size; i++) {
        if (this->GetInventory().m_items[i].IsKey())
            break;
    }

    return i;
}

Inventory Unit::GetInventory() {

    return this->m_inventory;
}

void Unit::AddInventory(Inventory inventory) {
    int size = int(inventory.m_items.size());
    int i;

    for (i = 0; i < size; i++) {
        this->AddItemToInventory(inventory.m_items[i]);
    }
}

void Unit::DropItem() {

    this->m_item = Item();
}

Item Unit::GetItem() {

    return this->m_item;
}

bool Unit::HasKey() {
    int size = int(this->GetInventory().m_items.size());
    int i;

    for (i = 0; i < size; i++) {
        if (this->GetInventory().m_items[i].IsKey())
            return true;
    }

    return false;
}

void Unit::SetName(std::string name) {

    this->m_name = name;
}

std::string Unit::GetName() {

    return this->m_name;
}

void Unit::SetId(int id) {

    this->m_id = id;
}

int Unit::GetId() {

    return this->m_id;
}

void Unit::SetArmor(int armor) {

    this->m_armor = armor;
}

int Unit::GetArmor() {

    return this->m_armor;
}

void Unit::SetAttack(int attack) {

    this->m_attack = attack;
}

int Unit::GetAttack() {

    return this->m_attack;
}

void Unit::SetHealth(int health) {

    this->m_health = health;
}

int Unit::GetHealth() {

    return this->m_health;
}

void Unit::SetRole(int lvl) {

    this->m_role = lvl;
}

int Unit::GetRole() {

    return this->m_role;
}

int Unit::GetCurrentRoom() {

    return this->m_currentRoom;
}

void Unit::SetCurrentRoom(int roomId) {

    this->m_currentRoom = roomId;
}

bool Unit::IsBoss() {

    return this->GetRole() == this->ROLE_BOSS;
}

bool Unit::IsMonster() {

    return this->GetRole() == this->ROLE_MOB;
}

Unit& Unit::operator=(Unit rightUnit) {
    this->SetName(rightUnit.GetName());
    this->SetId(rightUnit.GetId());
    this->SetRole(rightUnit.GetRole());
    this->SetHealth(rightUnit.GetHealth());
    this->SetAttack(rightUnit.GetAttack());
    this->SetArmor(rightUnit.GetArmor());

    return *this;
}