//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include "../include/FightCalculator.h"

FightCalculator::FightCalculator() {

}

int FightCalculator::GetBattleResult(Unit unitAttack, Unit unitDefend) {

    bool endBattle = false;

    while (!endBattle) {

        unitDefend.SetHealth(unitDefend.GetHealth() + unitDefend.GetArmor() - unitAttack.GetAttack());

        if (unitDefend.GetHealth() <= 0) {

            this->SetLeftHP(unitAttack.GetHealth());
            endBattle = true;
            this->m_winnerOfTheBattle = 1;
        }

        unitAttack.SetHealth(unitAttack.GetHealth() + unitAttack.GetArmor() - unitDefend.GetAttack());

        if (unitAttack.GetHealth() <= 0) {

            endBattle = true;
            this->SetLeftHP(unitDefend.GetHealth());
            this->m_winnerOfTheBattle = 2;
        }
    }

    return this->m_winnerOfTheBattle;
}

void FightCalculator::SetLeftHP(int leftHP) {

    this->m_leftHP = leftHP;
}

int FightCalculator::GetLeftHP() {

    return this->m_leftHP;
}