//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include "../include/Inventory.h"

Inventory::Inventory() {

}

Inventory::Inventory(int limit) {
    this->SetLimit(limit);
}

void Inventory::SetLimit(int limit) {

    this->m_limit = limit;
}

int Inventory::GetLimit() {

    return this->m_limit;
}

void Inventory::AddItem(Item item) {

    this->m_items.push_back(item);
}

bool Inventory::IsEmpty() {
    return this->m_items.size() == 0;
}

void Inventory::DropItem(Item item) {

    int size = int(this->m_items.size());
    int i;

    for (i = 0; i < size; i++) {
        if (this->m_items[i].GetId() == item.GetId())  {
            break;
        }
    }

    this->m_items.erase(m_items.begin() + i);
}
