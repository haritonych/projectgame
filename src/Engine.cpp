//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include "../include/Engine.h"
#include "../include/Room.h"
#include "../include/DataBase.h"
#include "../include/Inventory.h"
#include "../include/FightCalculator.h"
#include <iostream>
#include <fstream>


Engine::Engine() {

    this->PopulateRooms();
    this->RenderStartGame();

    while (this->m_gameState) {

        this->Step();
    }
}

Engine::~Engine() {}

void Engine::RenderStartGame() {

    int casePlayer;
    std::string playerName;

    std::cout << "<<-------------------------------------------------------->>" << std::endl;
    std::cout << "<|                                                        |>" << std::endl;
    std::cout << "<|                  Unnamed Game v1.0                     |>" << std::endl;
    std::cout << "<|                                                        |>" << std::endl;
    std::cout << "<<-------------------------------------------------------->>" << std::endl;
    std::cout << " __________________________________________________________ " << std::endl;
    std::cout << "|                       MAIN MENU                          |" << std::endl;
    std::cout << "|__________________________________________________________|" << std::endl;
    std::cout << "|                                                          |" << std::endl;
    std::cout << "|                     1. Start Game                        |" << std::endl;
    std::cout << "|                     2. About Creators                    |" << std::endl;
    std::cout << "|                     0. Exit                              |" << std::endl;
    std::cout << "|__________________________________________________________|" << std::endl;
    std::cin >> casePlayer;

    switch (casePlayer) {
        case 0:
            this->m_gameState = false;
            return;
        case 1:
            this->m_gameState = true;
            break;
        case 2:
            this->m_gameState = true;
            this->RenderAboutCreators();
            break;
        default:
            this->m_gameState = false;
            return;
    }

    std::cout << "\n|   Enter name of your hero:                                |" << std::endl;
    std::cin >> playerName;

    this->hero.CreateHero(playerName);
    this->m_rooms[this->hero.GetCurrentRoom()].SetOpen(true);

    std::cout << "\n<<-------------------------------------------------------->>" << std::endl;
    std::cout << "<<----------------------GAME STARTED---------------------->>" << std::endl;
    std::cout << "<<-------------------------------------------------------->>\n" << std::endl;

    this->m_phase = Engine::CHOSE_ACTION_PHASE;
}

void Engine::Step() {

    switch (this->m_phase) {
        case Engine::CHOSE_ACTION_PHASE:
            this->RenderChoiceActionPhase();
            break;
        case Engine::FIGHT_ACTION_PHASE:
            this->RenderFightAction();
            break;
        case Engine::SHIFT_ROOM_ACTION_PHASE:
            this->RenderShiftRoom();
            break;
        case Engine::INVENTORY_ACTION_PHASE:
            this->RenderInventory();
            break;
        case SHOW_MAP_ROOMS:
            this->RenderMap();
            break;
        case Engine::GAME_WIN:
            this->RenderGameWin();
            this->m_gameState = false;
            return;
        case Engine::EXIT_GAME_ACTION:
            this->m_gameState = false;
            return;
        default:
            this->RenderChoiceActionPhase();
            break;
    }
}

void Engine::RenderChoiceActionPhase() {
    int userResponse;

    this->RenderInfoPlayer();

    std::cout << "<<------HERO MENU------>>" << std::endl;
    std::cout << "1. Fight with someone" << std::endl;
    std::cout << "2. Inventory" << std::endl;
    std::cout << "3. Shift room" << std::endl;
    std::cout << "4. Map" << std::endl;
    std::cout << "0. End Game" << std::endl;
    std::cin >> userResponse;

    switch (userResponse) {
        case 0:
            this->m_phase = Engine::EXIT_GAME_ACTION;
            break;
        case 1:
            this->m_phase = Engine::FIGHT_ACTION_PHASE;
            break;
        case 2:
            this->m_phase = Engine::INVENTORY_ACTION_PHASE;
            break;
        case 3:
            this->m_phase = Engine::SHIFT_ROOM_ACTION_PHASE;
            break;
        case 4:
            this->m_phase = Engine::SHOW_MAP_ROOMS;
            break;
        default:
            this->m_phase = Engine::CHOSE_ACTION_PHASE;
            break;
    }
}

void Engine::RenderInventory() {
    int userResponse;
    std::cout << "                    Let's choose an item to wear                   " << std::endl;
    std::cout << "Inventory: " << std::endl;
    std::cout << this->hero.RenderInventory() << std::endl;
    std::cout << "0. Close" << std::endl;
    std::cin >> userResponse;

    if (userResponse == 0) {
        this->m_phase = Engine::CHOSE_ACTION_PHASE;
    } else {
        this->hero.EquipItemFromInventory(userResponse);
        this->hero.DropItemFromInventory(this->hero.GetItem());
        this->m_phase = Engine::CHOSE_ACTION_PHASE;
    }
}

void Engine::RenderShiftRoom() {
    int userResponse;

    std::cout << "                    Which room should we go to?                    " << std::endl;
    std::cout << " _________________________________________________________________ " << std::endl;
    std::cout << "*|                       Icecrown Citadel                          |*" << std::endl;
    std::cout << "*|_________________________________________________________________|*" << std::endl;
    std::cout << "*|                                                                 |*" << std::endl;

    if (this->hero.GetCurrentRoom() == 0 || this->hero.GetCurrentRoom() + 1 == this->m_rooms.size()) {
        if (this->hero.GetCurrentRoom() == 0) {
            std::cout << "*| " << this->m_rooms[this->hero.GetCurrentRoom() + 1].GetId() << ". "
                      << this->m_rooms[this->hero.GetCurrentRoom() + 1].GetName() << std::endl;
            std::cout << "*|    *Description: " << this->m_rooms[this->hero.GetCurrentRoom() + 1].GetDescription() << std::endl;
        } else {
            std::cout << "*| " << this->m_rooms[this->hero.GetCurrentRoom() - 1].GetId() << ". "
                      << this->m_rooms[this->hero.GetCurrentRoom() - 1].GetName() << std::endl;
            std::cout << "*|    *Description: " << this->m_rooms[this->hero.GetCurrentRoom() - 1].GetDescription() << std::endl;
        }
    } else {
        std::cout << "*| " << this->m_rooms[this->hero.GetCurrentRoom() - 1].GetId() << ". "
                  << this->m_rooms[this->hero.GetCurrentRoom() - 1].GetName() << std::endl;
        std::cout << "*|    *Description: " << this->m_rooms[this->hero.GetCurrentRoom() - 1].GetDescription() << std::endl;
        std::cout << "*| " << this->m_rooms[this->hero.GetCurrentRoom() + 1].GetId() << ". "
                  << this->m_rooms[this->hero.GetCurrentRoom() + 1].GetName() << std::endl;
        std::cout << "*|    *Description: " << this->m_rooms[this->hero.GetCurrentRoom() + 1].GetDescription() << std::endl;
    }
    std::cout << "*|                                                                 |*" << std::endl;
    std::cout << "*|_________________________________________________________________|*" << std::endl;
    std::cout << "0. Close" << std::endl;
    std::cin >> userResponse;

    if (userResponse == 0) {
        this->m_phase = Engine::CHOSE_ACTION_PHASE;
    } else {
        if (this->hero.HasKey() || this->m_rooms[userResponse - 1].IsOpen()) {
            if (this->hero.HasKey() && !this->m_rooms[userResponse - 1].IsOpen()) {
                this->hero.DropItemFromInventory(this->hero.GetInventory().m_items[this->hero.GetKeyFromInventory()]);
            }
            this->m_rooms[userResponse - 1].SetOpen(true);
            this->hero.SetCurrentRoom(userResponse - 1);
        } else {
            std::cout << " _________________________________________________________________ " << std::endl;
            std::cout << "*|         You don't have a KEY to go to the next room :(          |*" << std::endl;
            std::cout << "*|_________________________________________________________________|*" << std::endl;
        }
        this->m_phase = Engine::CHOSE_ACTION_PHASE;
    }

}

void Engine::RenderFightAction() {

    int userResponse;

    std::cout << "<<------FIGHT WITH SOMEONE------>>" << std::endl;
    std::cout << this->m_rooms[this->hero.GetCurrentRoom()].RenderUnits() << std::endl;
    std::cout << "0. Close" << std::endl;
    std::cin >> userResponse;

    if (userResponse == 0) {
        this->m_phase = Engine::CHOSE_ACTION_PHASE;
    } else {
        FightCalculator battle;
        if (battle.GetBattleResult(this->hero, this->m_rooms[this->hero.GetCurrentRoom()].GetUnitById(userResponse)) == 1) {
            this->hero.SetHealth(battle.GetLeftHP());
            std::cout << "|                    !!!VICTORY!!!                         |" << std::endl;
            this->hero.AddInventory(this->m_rooms[this->hero.GetCurrentRoom()].GetUnitById(userResponse).GetInventory());
            if (this->m_rooms[this->hero.GetCurrentRoom()].GetId() == 13) {
                this->m_phase = Engine::GAME_WIN;
            } else {
                this->m_phase = Engine::CHOSE_ACTION_PHASE;
            }
            this->m_rooms[this->hero.GetCurrentRoom()].RemoveUnit(userResponse);

            return;
        } else {
            std::cout << "|                     !!!DEFEAT!!!                         |\n" << std::endl;
            this->m_phase = Engine::EXIT_GAME_ACTION;

            return;
        }

//        if (this->m_rooms[this->hero.GetCurrentRoom()].GetUnitById(userResponse).IsBoss()
//                && this->m_rooms[this->hero.GetCurrentRoom()].GetCountMob() > 1) {
//
//            std::cout << "|    You must kill all mobs before fight with BOSS!    |" << std::endl;
//            this->m_phase = Engine::FIGHT_ACTION_PHASE;
//
//            return;
//        } else {
//            if (battle.GetBattleResult(this->hero, this->m_rooms[this->hero.GetCurrentRoom()].GetUnitById(userResponse)) == 1) {
//                this->hero.SetHealth(battle.GetLeftHP());
//                std::cout << "|                    !!!VICTORY!!!                         |" << std::endl;
//                this->hero.AddInventory(this->m_rooms[this->hero.GetCurrentRoom()].GetUnitById(userResponse).GetInventory());
//                this->m_rooms[this->hero.GetCurrentRoom()].RemoveUnit(userResponse);
//                this->m_phase = Engine::CHOSE_ACTION_PHASE;
//
//                return;
//            } else {
//                std::cout << "|                     !!!DEFEAT!!!                         |\n" << std::endl;
//                this->m_phase = Engine::EXIT_GAME_ACTION;
//
//                return;
//            }
//        }
    }
}

void Engine::RenderInfoPlayer() {

    std::cout << "<<<------------------------------------------------------>>>" << std::endl;
    std::cout << "INFO ABOUT HERO\n" << std::endl;
    std::cout << "Hero name: " << this->hero.GetName() << std::endl;
    std::cout << "\tHealth: " << this->hero.GetHealth() << std::endl;
    std::cout << "\tArmor: " << this->hero.GetArmor() << std::endl;
    std::cout << "\tAttack: " << this->hero.GetAttack() << std::endl;
    std::cout << "----------------" << std::endl;
    std::cout << "Item equipped: " << std::endl;
    std::cout << this->hero.GetItem().GetShortInfo() << std::endl;
    std::cout << "----------------" << std::endl;
    std::cout << "Inventory: " << std::endl;
    std::cout << this->hero.RenderInventory() << std::endl;
    std::cout << "----------------" << std::endl;
    std::cout << "Current room: " << std::endl;
    std::cout << "Room name: " << this->m_rooms[this->hero.GetCurrentRoom()].GetName() << std::endl;
    std::cout << "\tDescription: " << this->m_rooms[this->hero.GetCurrentRoom()].GetDescription() << std::endl;
    std::cout << "<<<------------------------------------------------------>>>" << std::endl;
}

void Engine::RenderMap() {

    int userResponse;

    std::cout << " _________________________________________________________________ " << std::endl;
    std::cout << "*|                       Icecrown Citadel                          |*" << std::endl;
    std::cout << "*|_________________________________________________________________|*" << std::endl;
    std::cout << "*|                                                                 |*" << std::endl;

    for (int i = 0; i < this->GetSizeRooms(); ++i) {
        std::cout << "*| " << this->m_rooms[i].GetId() << ". " << this->m_rooms[i].GetName() << std::endl;
        std::cout << "*|    *Description: " << this->m_rooms[i].GetDescription() << std::endl;

    }
    std::cout << "*|                                                                 |*" << std::endl;
    std::cout << "*|_________________________________________________________________|*" << std::endl;
    std::cout << "0. Close" << std::endl;
    std::cin >> userResponse;

    this->m_phase = Engine::CHOSE_ACTION_PHASE;
}

void Engine::PopulateRooms() {

    this->m_rooms = this->dataBase.GetRooms();
    std::vector<Unit> units = this->dataBase.GetUnits();
    std::vector<Item> items = this->dataBase.GetItems();
    int sizeUnits = int(units.size());
    int sizeItems = int(items.size());
    int sizeRooms = int(this->GetSizeRooms());

    for (int n = 0; n < sizeRooms; ++n) {
        for (int i = 0; i < sizeUnits; ++i) {
            if (units[i].GetCurrentRoom() == this->m_rooms[n].GetId()) {
                for (int j = 0; j < sizeItems; ++j) {
                    if (items[j].GetIdUnit() == units[i].GetId()) {
                        units[i].AddItemToInventory(items[j]);
                    }
                }
                this->m_rooms[n].AddUnit(units[i]);
            }
        }
    }
}

void Engine::RenderGameWin() {

    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *  " << std::endl;
    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "  *   *   *   *   *   *   *  CONGRATULATION  *   *   *   *   *   *   *   *   " << std::endl;
    std::cout << "*   *   *   *   *   *   *   *   YOU WON!!! *   *   *   *   *   *   *   *   * " << std::endl;
    std::cout << "  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *  " << std::endl;
    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *  " << std::endl;
}

void Engine::RenderAboutCreators() {
    int userResponse;

    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *  " << std::endl;
    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "                             Kharytonov Danylo                               " << std::endl;
    std::cout << "  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *  " << std::endl;
    std::cout << "                              Eliška Macková                                 " << std::endl;
    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "  *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *  " << std::endl;
    std::cout << "*   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *   *" << std::endl;
    std::cout << "0. Close" << std::endl;
    std::cin >> userResponse;

    if (userResponse) {
        this->RenderStartGame();
    }
}

int Engine::GetSizeRooms() {

    return int(this->m_rooms.size());
}
