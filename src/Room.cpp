//
// Created by Danylo Kharytonov on 15.12.2020.
//

#include "../include/Room.h"

Room::Room() {

}

Room::Room(int id, int countMob, std::string description, std::string name) {
    this->SetId(id);
    this->SetCountMob(countMob);
    this->SetDescription(description);
    this->SetName(name);
    this->SetOpen(false);
}

void Room::SetOpen(bool isOpen) {

    this->m_open = isOpen;
}

bool Room::IsOpen() {

    return this->m_open;
}

int Room::GetId() {
    return this->m_id;
}

void Room::SetId(int id) {
    this->m_id = id;
}

std::string Room::GetDescription() {

    return this->m_description;
}

void Room::SetDescription(std::string description) {

    this->m_description = description;
}

int Room::GetCountMob() {

    return this->m_countMob;
}

void Room::SetCountMob(int countMob) {

    this->m_countMob = countMob;
}


std::string Room::GetName() {

    return this->m_name;
}

void Room::SetName(std::string name) {

    this->m_name = name;
}

void Room::AddUnit(Unit unit) {

    this->m_units.push_back(unit);
}

void Room::RemoveUnit(int unitId) {

    m_units.erase(m_units.begin() + this->GetUnitId(unitId));
    this->SetCountMob(this->GetCountMob() - 1);
}

int Room::GetUnitId(int idUnit) {

    int size = int(this->m_units.size());
    int i;

    for (i = 0; i < size; i++) {
        if (this->m_units[i].GetId() == idUnit)  {
            break;
        }
    }

    return i;
}

Unit Room::GetUnitById(int idUnit) {

    int size = int(this->m_units.size());
    int i;

    for (i = 0; i < size; i++) {
        if (this->m_units[i].GetId() == idUnit)  {
            break;
        }
    }

    return this->m_units[i];
}

std::string Room::RenderUnits() {

    int size = this->m_units.size();
    std::string info;

    info = "";
    for (int i = 0; i < size; ++i) {
        if (this->m_units[i].IsMonster())
            info += "\nMonster----> (" + std::to_string(this->m_units[i].GetId()) + ") " + this->m_units[i].GetName();
        else
            info += "\nBOSS-------> (" + std::to_string(this->m_units[i].GetId()) + ") " + this->m_units[i].GetName();

        info += "\n\t--Health--> " + std::to_string(this->m_units[i].GetHealth());
        info += "\n\t--Attack--> " + std::to_string(this->m_units[i].GetAttack());
        info += "\n\t--Armor---> " + std::to_string(this->m_units[i].GetArmor());
        info += "\n\t--Inventory---> " + this->m_units[i].RenderInventory();
    }

    return info;
}


Room& Room::operator=(Room rightRoom) {
    this->SetName(rightRoom.GetName());
    this->SetCountMob(rightRoom.GetCountMob());
    this->SetDescription(rightRoom.GetDescription());
    this->SetId(rightRoom.GetId());

    return *this;
}