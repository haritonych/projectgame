//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_DATABASE_H
#define DUNGEON_DATABASE_H

#include "Item.h"
#include "Unit.h"
#include "Room.h"
#include <vector>
#include <sqlite3.h>

class DataBase {
public:
    DataBase();
    ~DataBase();

    std::vector<Room> GetRooms();
    std::vector<Item> GetItems();
    std::vector<Unit> GetUnits();
private:
    sqlite3 *db;
};

#endif //DUNGEON_DATABASE_H
