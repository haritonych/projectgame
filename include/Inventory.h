//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_INVENTORY_H
#define DUNGEON_INVENTORY_H

#include <array>
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

class Inventory {
public:
    Inventory();
    Inventory(int);
    ~Inventory() {};

    std::vector<Item> m_items;

    void SetLimit(int);
    int GetLimit();
    void AddItem(Item);
    void DropItem(Item);
    bool IsEmpty();

private:
    static const int LIMIT_FOR_MOB = 2;
    static const int LIMIT_FOR_BOSS = 4;
    static const int LIMIT_FOR_HERO = 10;

    int m_limit;
};
#endif //DUNGEON_INVENTORY_H
