//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_FIGHTCALCULATOR_H
#define DUNGEON_FIGHTCALCULATOR_H

#include "Unit.h"

class FightCalculator {
public:
    FightCalculator();
    ~FightCalculator() {};

    void SetLeftHP(int);
    int GetLeftHP();

    int GetBattleResult(Unit, Unit);

private:
    int m_leftHP;
    int m_winnerOfTheBattle;
};

#endif //DUNGEON_FIGHTCALCULATOR_H
