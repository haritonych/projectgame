//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_ITEM_H
#define DUNGEON_ITEM_H

#include <string>

class Item {

public:
    Item();
    Item(int, int, int, int, std::string, std::string, bool, int);
    ~Item();

    Item& operator = (Item);

    void SetName(std::string);
    void SetPlusAttack(int);
    void SetPlusHealth(int);
    void SetPlusArmor(int);
    void SetId(int);
    void SetDescription(std::string);
    void SetKey(bool);
    void SetIdUnit(int);
    std::string GetName();
    int GetPlusAttack();
    int GetPlusHealth();
    int GetPlusArmor();
    int GetId();
    int GetIdUnit();
    bool IsKey();
    std::string GetDescription();
    std::string GetShortInfo();
    bool IsEmpty();

private:
    int m_id;
    std::string m_name;
    int m_plusAttack;
    int m_plusHealth;
    int m_plusArmor;
    bool m_key;
    int m_id_unit;
    std::string m_description;
};

#endif //DUNGEON_ITEM_H
