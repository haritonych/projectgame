//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_UNIT_H
#define DUNGEON_UNIT_H

#include <array>
#include <string>
#include <vector>
#include "Item.h"
#include "Inventory.h"

class Unit {
public:
    Unit();
    Unit(int, int, int, int, int, int, std::string);
    ~Unit() {};

    Unit& operator=(Unit);

    void SetName(std::string);
    void SetArmor(int);
    void SetId(int);
    void SetHealth(int);
    void SetAttack(int);
    void SetRole(int);
    bool SetItem(Item);
    std::string GetName();
    int GetArmor();
    int GetHealth();
    int GetAttack();
    int GetRole();
    int GetId();
    Item GetItem();
    bool HasKey();
    void DropItem();
    void AddItemToInventory(Item);
    void AddInventory(Inventory);
    Inventory GetInventory();
    int GetKeyFromInventory();
    void DropItemFromInventory(Item);
    void EquipItemFromInventory(int);
    std::string RenderInventory();
    void CreateHero(std::string);
    void SetCurrentRoom(int);
    int GetCurrentRoom();
    Item GetItemById(int);
    bool IsMonster();
    bool IsBoss();

private:
    Inventory m_inventory;

    int m_id;
    std::string m_name;
    int m_armor;
    int m_health;
    int m_attack;
    int m_role;
    Item m_item;
    int m_currentRoom;

    static const int ROLE_MOB = 1;
    static const int ROLE_BOSS = 2;
    static const int ROLE_HERO = 3;

};

#endif //DUNGEON_UNIT_H
