//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_ENGINE_H
#define DUNGEON_ENGINE_H

#include "Item.h"
#include "DataBase.h"
#include "Room.h"

class Engine {
public:
    Engine();
    ~Engine();

private:
    static const int CHOSE_ACTION_PHASE = 0;
    static const int FIGHT_ACTION_PHASE = 1;
    static const int SHIFT_ROOM_ACTION_PHASE = 2;
    static const int INVENTORY_ACTION_PHASE = 3;
    static const int SHOW_MAP_ROOMS = 4;
    static const int GAME_WIN = 5;
    static const int EXIT_GAME_ACTION = 999;

    DataBase dataBase;
    Unit hero;

    std::vector<Room> m_rooms;

    bool m_gameState;
    int m_phase;

    void PopulateRooms();
    void Step();
    void RenderStartGame();
    void RenderChoiceActionPhase();
    void RenderInfoPlayer();
    void RenderFightAction();
    void RenderMap();
    int GetSizeRooms();
    void RenderGameWin();
    void RenderShiftRoom();
    void RenderInventory();
    void RenderAboutCreators();
};


#endif //DUNGEON_ENGINE_H
