//
// Created by Danylo Kharytonov on 15.12.2020.
//

#ifndef DUNGEON_ROOM_H
#define DUNGEON_ROOM_H

#include <string>
#include "Unit.h"


class Room {
public:
    Room();
    Room(int, int, std::string, std::string);
    ~Room() {};

    Room& operator=(Room);

    void SetId(int);
    void SetName(std::string);
    void SetDescription(std::string);
    void SetCountMob(int);
    void SetOpen(bool);
    int GetId();
    int GetCountMob();
    std::string GetDescription();
    std::string GetName();
    void AddUnit(Unit);
    void RemoveUnit(int);
    Unit GetUnitById(int);
    int GetUnitId(int);
    std::string RenderUnits();
    bool IsOpen();

private:
    int m_id;
    std::string m_description;
    std::string m_name;
    int m_countMob;
    bool m_open;

    std::vector<Unit> m_units;
};

#endif //DUNGEON_ROOM_H
